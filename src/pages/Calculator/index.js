import React, { useState, useEffect } from 'react'
import './style.css';
import starWarsLogo from '../../assets/Star_Wars_Yellow_Logo.svg.png';
import api from '../../services/api';
import { MdArrowForward, MdArrowBack } from 'react-icons/md'
import moment from 'moment'

export default function Calculator() {
    const [distance, setDistance] = useState('');
    const [starships, setStarships] = useState([]);
    const [page, setPage] = useState(1);
    const [previous, setPrevious] = useState('');
    const [next, setNext] = useState('');

    useEffect(() => {
        api.get('starships', { params: { page: page } })
            .then(response => {
                setPrevious(response.data.previous);
                setNext(response.data.next);
                setStarships(response.data.results);
            })
    }, [page]);

    function validateNumber(e) {
        var theEvent = e || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }

    function handleForm(event) {
        event.preventDefault();
    }

    function convertToHours(consumables) {
        let time = consumables.split(' ');
        return moment.duration(time[0], time[1]).asHours();
    }

    function testBackward() {
        if (previous) {
            setPage(page - 1)
        }
    }

    function testFoward() {
        if (next) {
            setPage(page + 1)
        }
    }

    function getResult(starship) {
        let result = parseInt(distance / (starship.MGLT * (convertToHours(starship.consumables))))
        if(isNaN(result)){
            return `Can't Calculate`
        } else {
            return result;
        }
    }

    return (
        <div className="calculator-container">
            <section className="form">
                <img className="logo" src={starWarsLogo} alt="Star Wars Logo" />
                <form onSubmit={handleForm}>
                    <h3>Distance of your Journey</h3>
                    <input
                        placeholder="Distance on MGLT"
                        value={distance}
                        type="text"
                        onChange={e => setDistance(e.target.value)}
                        onKeyPress={e => validateNumber(e)}
                        onPaste={e => validateNumber(e)}
                        onDrop={e => validateNumber(e)}
                        onDrag={e => validateNumber(e)}
                        maxLength={30}
                    />
                    <ul>
                        {starships.map(starship =>
                            (
                                <ul key={starship.name}>
                                    <li>Starship Name:</li>
                                    <li>{starship.name}</li>
                                    <li>Stops Necessary:</li>
                                    <li>{getResult(starship)}</li>
                                </ul>
                            )
                        )}
                    </ul>
                    <nav>
                        <MdArrowBack onClick={e => testBackward()} />
                        <MdArrowForward onClick={e => testFoward()} />
                    </nav>
                </form>
            </section>
        </div>
    );
}